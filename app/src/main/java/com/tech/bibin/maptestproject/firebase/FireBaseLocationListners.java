package com.tech.bibin.maptestproject.firebase;

import com.google.firebase.database.DataSnapshot;

/**
 * Created by bibin.b on 9/19/2017.
 */

public interface FireBaseLocationListners {
    void mLocationData(DataSnapshot dataSnapshot);

    void onErrorData(String error);
}
