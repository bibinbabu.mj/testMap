package com.tech.bibin.maptestproject;

import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tech.bibin.maptestproject.firebase.FireBaseLocationListners;
import com.tech.bibin.maptestproject.firebase.FireBaseTruckLocationListners;
import com.tech.bibin.maptestproject.utils.PrefManager;

import java.util.ArrayList;

/**
 * Created by bibin.b on 1/9/2018.
 */

public class MainMapActivity extends AppCompatActivity implements FireBaseLocationListners,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, OnMapReadyCallback {
    private static final String TAG = "GoogleeMaps";
    private FireBaseTruckLocationListners fireBaseTruckLocationListners;
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Marker marker;
    private LocationProvider mLocationProvider;
    private Locationss locations;
    private PrefManager prefManager;
    private MarkerOptions markerOptions;
    private LatLng userLatlong;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        prefManager = PrefManager.getInstance(this);
        fireBaseTruckLocationListners = new FireBaseTruckLocationListners(this);
        locations = new Locationss();
        markerOptions = new MarkerOptions();
        mCallBack();
        setUpMapIfNeeded();
        mTimeInterVel();
    }

    private void mCallBack() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void mLocationData(DataSnapshot dataSnapshot) {
        ArrayList<Locationss> models = new ArrayList<>();
        models.clear();
        mMap.clear();
        setUpMapIfNeeded();
        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
            Locationss locationModel = postSnapshot.getValue(Locationss.class);
            models.add(locationModel);
        }
        addMarker(models);
    }

    private void addMarker(ArrayList<Locationss> models) {
        if (models.size() == 0) {
            return;
        }

        for (int i = 0; i < models.size(); i++) {
            Double Latitude = models.get(i).getLatitude();
            Double Longitude = models.get(i).getLongitude();
            LatLng latLng = new LatLng(Latitude, Longitude);

            // marker = mMap.addMarker(new MarkerOptions().position(new LatLng(latLng.latitude, latLng.longitude)));
            marker = mMap.addMarker(markerOptions.position(latLng));
            marker.setPosition(latLng);
            /// position(new LatLng(31.647316, 74.763791));
            mMap.getUiSettings().setMapToolbarEnabled(false);
            mMap.getUiSettings().setZoomControlsEnabled(true);
        }
    }

    @Override
    public void onErrorData(String error) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            mGoogleApiClient.connect();
        } else {
            handleNewLocation(location);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    private void setUpMapIfNeeded() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        /*if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
        mGoogleApiClient.connect();*/
    }

    private void mTimeInterVel() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); //
    }

    @Override
    public void onLocationChanged(Location location) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location locations = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            handleNewLocation(location);
        }
    }

    private void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        locations.setLatitude(currentLatitude);
        locations.setLongitude(currentLongitude);
        saveUserData(locations);
        userLatlong = new LatLng(currentLatitude, currentLongitude);
        Log.d(TAG, "handleNewLocation: " + currentLatitude);
        Log.d(TAG, "handleNewLocation: " + currentLongitude);
    }

    private void saveUserData(Locationss locations) {
        if (prefManager.isUserRegister()) {
            mAddFireBaser(prefManager.getUuid(), locations);
        } else {
            final DatabaseReference pushOrder = FirebaseDatabase.getInstance().getReference().child("locations").push();
            prefManager.setUuid(pushOrder.getKey());
            prefManager.setUserRegistrer(true);
            mAddFireBaser(pushOrder.getKey(), locations);
        }
    }

    private void mAddFireBaser(String uuid, Locationss model) {
        FirebaseDatabase.getInstance().getReference()
                .child("locations").child(uuid)
                .setValue(model, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError == null) {
                        }
                    }
                });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        fireBaseTruckLocationListners.mLocationData();
    }
}
