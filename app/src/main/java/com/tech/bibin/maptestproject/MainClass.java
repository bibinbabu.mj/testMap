package com.tech.bibin.maptestproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.model.MarkerOptions;
import com.tech.bibin.maptestproject.firebase.FireBaseTruckLocationListners;
import com.tech.bibin.maptestproject.googlemap.BackgroundLocationService;
import com.tech.bibin.maptestproject.googlemap.LocationLoggerServiceManager;
import com.tech.bibin.maptestproject.utils.PrefManager;

/**
 * Created by bibin.b on 1/10/2018.
 */

public class MainClass extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Intent intent=new Intent(this,BackgroundLocationService.class);
        startService(intent);
    }
}
