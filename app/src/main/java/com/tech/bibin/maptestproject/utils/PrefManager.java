package com.tech.bibin.maptestproject.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Created by bibin.b on 4/4/2017.
 */

public class PrefManager {

    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG_NAME = "fresco.prefs";
    private static PrefManager instance;
    private SharedPreferences prefs;
    private static final String USER_INFO = "user_info";
    private static final String IS_REGISTRED = "is_registred";

    // ===========================================================
    // Constructors
    // ===========================================================

    private PrefManager(Context context) {
        prefs = context.getSharedPreferences(TAG_NAME, Context.MODE_PRIVATE);
    }

    public static PrefManager getInstance(Context context) {
        if (instance == null) {
            instance = new PrefManager(context);
        }
        return instance;
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================


    private void putString(String key, String value) {
        Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();

    }

    private void putInt(String key, int value) {
        Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    private void putBoolean(String key, boolean value) {
        Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    private void putLong(String key, long value) {
        Editor editor = prefs.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public SharedPreferences getSharedPreferences() {
        return prefs;
    }


    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public void setUserRegistrer(boolean value) {
        putBoolean("setUserRegistrer", value);
        if (!value) {
            // clear all values

        }

    }

    public boolean isUserRegister() {
        return prefs.getBoolean("setUserRegistrer", false);

    }

    public void setUuid(String value) {
        putString("setUserid", value);
    }

    public String getUuid() {
        return prefs.getString("setUserid", "");

    }

}
