package com.tech.bibin.maptestproject

import android.app.Application
import android.support.multidex.MultiDexApplication

import com.google.firebase.database.FirebaseDatabase

/**
 * Created by bibin.b on 8/23/2017.
 */

class AppClass : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        FirebaseDatabase.getInstance().setPersistenceEnabled(false)
    }
}

