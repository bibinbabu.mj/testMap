package com.tech.bibin.maptestproject.firebase;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
/***
 * Created by bibin.b on 9/19/2017.
 */

public class FireBaseTruckLocationListners implements ValueEventListener {
    private FireBaseLocationListners fireBaseLocationListners;

    public FireBaseTruckLocationListners(FireBaseLocationListners fireBaseLocationListners) {
        this.fireBaseLocationListners = fireBaseLocationListners;
    }


    private final DatabaseReference mLocationRef = FirebaseDatabase.getInstance().getReference();
            //.getReferenceFromUrl(ConfigFireBase.FIREBASE_URL + "truck_location");

    public void mLocationData() {
        mLocationRef.child("locations").addListenerForSingleValueEvent(this);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        if (dataSnapshot != null) {
            fireBaseLocationListners.mLocationData(dataSnapshot);
        } else {
            return;
        }

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        fireBaseLocationListners.onErrorData(databaseError.getMessage());
    }
}
