package com.tech.bibin.maptestproject;

import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tech.bibin.maptestproject.firebase.FireBaseLocationListners;
import com.tech.bibin.maptestproject.firebase.FireBaseTruckLocationListners;
import com.tech.bibin.maptestproject.utils.PrefManager;

import java.util.ArrayList;

/**
 * Created by bibin.b on 1/8/2018.
 */

public class GoogleeMaps extends AppCompatActivity implements LocationProvider.LocationCallback, OnMapReadyCallback, FireBaseLocationListners {
    private static final String TAG = "GoogleeMaps";
    private FireBaseTruckLocationListners fireBaseTruckLocationListners;
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Marker marker;
    private LocationProvider mLocationProvider;
    private Locationss locations;
    private PrefManager prefManager;
    private MarkerOptions markerOptions;
    private LatLng userLatlong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        prefManager = PrefManager.getInstance(this);
        fireBaseTruckLocationListners = new FireBaseTruckLocationListners(this);
        locations = new Locationss();
        markerOptions = new MarkerOptions();
        mLocationProvider = new LocationProvider(this, this);
        asPermission();
        setUpMapIfNeeded();


    }

    private void asPermission() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        mLocationProvider.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocationProvider.disconnect();
    }

    private void setUpMapIfNeeded() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        locations.setLatitude(currentLatitude);
        locations.setLongitude(currentLongitude);
        saveUserData(locations);
        userLatlong = new LatLng(currentLatitude, currentLongitude);
        Log.d(TAG, "handleNewLocation: " + currentLatitude);
        Log.d(TAG, "handleNewLocation: " + currentLongitude);
    }


    private void saveUserData(final Locationss model) {

        if (prefManager.isUserRegister()) {
            mAddFireBaser(prefManager.getUuid(), model);
        } else {
            final DatabaseReference pushOrder = FirebaseDatabase.getInstance().getReference().child("locations").push();
            prefManager.setUuid(pushOrder.getKey());
            prefManager.setUserRegistrer(true);
            mAddFireBaser(pushOrder.getKey(), model);
        }

    }

    private void mAddFireBaser(String uuid, Locationss model) {
        FirebaseDatabase.getInstance().getReference()
                .child("locations").child(uuid)
                .setValue(model, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError == null) {
                        }
                    }
                });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        fireBaseTruckLocationListners.mLocationData();
       /* if (userLatlong != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLatlong, 10));
        }*/

    }

    @Override
    public void mLocationData(DataSnapshot dataSnapshot) {
        ArrayList<Locationss> models = new ArrayList<>();
        models.clear();
        mMap.clear();
        setUpMapIfNeeded();
        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
            Locationss locationModel = postSnapshot.getValue(Locationss.class);
            models.add(locationModel);
        }
        addMarker(models);
    }

    private void addMarker(ArrayList<Locationss> models) {
        if (models.size() == 0) {
            return;
        }

        for (int i = 0; i < models.size(); i++) {
            Double Latitude = models.get(i).getLatitude();
            Double Longitude = models.get(i).getLongitude();
            LatLng latLng = new LatLng(Latitude, Longitude);

            // marker = mMap.addMarker(new MarkerOptions().position(new LatLng(latLng.latitude, latLng.longitude)));
            marker = mMap.addMarker(markerOptions.position(latLng));
            marker.setPosition(latLng);
            /// position(new LatLng(31.647316, 74.763791));
            mMap.getUiSettings().setMapToolbarEnabled(false);
            mMap.getUiSettings().setZoomControlsEnabled(true);

        }
    }

    @Override
    public void onErrorData(String error) {

    }
}