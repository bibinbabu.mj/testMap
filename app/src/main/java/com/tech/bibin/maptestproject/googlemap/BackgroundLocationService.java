package com.tech.bibin.maptestproject.googlemap;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tech.bibin.maptestproject.Locationss;
import com.tech.bibin.maptestproject.utils.PrefManager;

/**
 * Created by bibin.b on 1/10/2018.
 */

public class BackgroundLocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private static final String TAG = "BackgroundLocationServi";
    private Boolean servicesAvailable = false;
    private boolean mInProgress;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private PowerManager.WakeLock mWakeLock;
    IBinder mBinder = new LocalBinder();
    private PrefManager prefManager;
    private Locationss locations;

    @Override
    public void onCreate() {
        super.onCreate();
        mInProgress = false;
        locations = new Locationss();
        mCreateLocationRequest();
        setUpLocationClientIfNeeded();
        prefManager = PrefManager.getInstance(this);
    }

    private void setUpLocationClientIfNeeded() {
        if (mGoogleApiClient == null)
            buildGoogleApiClient();
    }

    protected synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient != null) {
            return;
        }
        this.mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void mCreateLocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10000)        // 10 seconds, in milliseconds
                .setFastestInterval(5000);
        servicesAvailable = servicesConnected();
    }

    private Boolean servicesConnected() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        } else {
            return false;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if (!servicesAvailable || mGoogleApiClient.isConnected() || mInProgress)
            return START_STICKY;
        setUpLocationClientIfNeeded();
        if (!mGoogleApiClient.isConnected() || !mGoogleApiClient.isConnecting() && !mInProgress) {
            mInProgress = true;
            mGoogleApiClient.connect();
        }

        return START_STICKY;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Intent intent = new Intent(this, LocationLoggerServiceManager.class);
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            mGoogleApiClient.connect();
        } else {
            handleNewLocation(location);
        }
       /* LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient,
                mLocationRequest, this);*/
    }

    private void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        locations.setLatitude(currentLatitude);
        locations.setLongitude(currentLongitude);
        saveUserData(locations);
        Log.d(TAG, "handleNewLocation: " + currentLatitude);
        Log.d(TAG, "handleNewLocation: " + currentLongitude);
    }

    private void saveUserData(Locationss locations) {
        if (prefManager.isUserRegister()) {
            mAddFireBaser(prefManager.getUuid(), locations);
        } else {
            final DatabaseReference pushOrder = FirebaseDatabase.getInstance().getReference().child("locations").push();
            prefManager.setUuid(pushOrder.getKey());
            prefManager.setUserRegistrer(true);
            mAddFireBaser(pushOrder.getKey(), locations);
        }
    }

    private void mAddFireBaser(String uuid, Locationss locations) {
        FirebaseDatabase.getInstance().getReference()
                .child("locations").child(uuid)
                .setValue(locations, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError == null) {
                        }
                    }
                });
    }

    @Override
    public void onConnectionSuspended(int i) {
        mInProgress = false;
        mGoogleApiClient = null;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mInProgress = false;
        if (connectionResult.hasResolution()) {

        } else {

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);

    }

    public class LocalBinder extends Binder {
        public BackgroundLocationService getServerInstance() {
            return BackgroundLocationService.this;
        }
    }

    @Override
    public void onDestroy() {
        this.mInProgress = false;

        if (this.servicesAvailable && this.mGoogleApiClient != null) {
            this.mGoogleApiClient.unregisterConnectionCallbacks(this);
            this.mGoogleApiClient.unregisterConnectionFailedListener(this);
            this.mGoogleApiClient.disconnect();
            this.mGoogleApiClient = null;
        }
        super.onDestroy();
    }
}
